<?php
/**
 * @package F7 Indexer
 * @version 1.0c
 * @author Fahad Hossain(@devilv15)
 * @license GPLv3
 * Created originally on 14th June 2014 at GMT 11:30 AM
 */
/**********************************/
$defaultPath = '.';
/**********************************/

/**
 * Returns the content of the entered directory with formatting
 * @param string $path The directory to be listed
 * @param string $defPath The default path [optional]
 */

function showContent($path, $defPath = '.')
{
    if ($handle = opendir($path)) {
        $up = substr($path, 0, (strrpos(dirname($path . "/."), "/")));
        if ($path != $defPath) {
            ?>
            <tr>
                <td colspan="3"><img src='_inc/up.png' width='16' height='16' alt='up'/>
                    <a class="btn-sm btn-success" href="<?php echo $_SERVER['PHP_SELF']; ?>?path=<?php echo $up; ?>">Go one level Up</a>
                </td>
            </tr>
        <?php
        }

        while (false !== ($file = readdir($handle))) {
            if ($file != "." && $file != "..") {
                $fName = $file;
                $file = $path . '/' . $file;
                if (is_file($file)) {
                    $kBs = (filesize($file) - (filesize($file) % 1000)) / 1000;
                    $fKbs = ($kBs - ($kBs % 1000)) / 1000 . '.' . ($kBs % 1000);
                    ?>
                    <tr>
                        <td>
                            <img src='_inc/file.png' width='16' height='16' alt='file'/>
                            <a class="btn-sm btn-primary"
                               href="<?php echo htmlspecialchars($file, ENT_QUOTES, "ISO-8859-1"); ?>"><?php echo $fName; ?></a>
                        </td>
                        <td><?php echo date('d/m H:i', filemtime($file)); ?></td>
                        <td class='fsize'><?php echo $fKbs; ?> M</td>
                    </tr>
                <?php
                } elseif (is_dir($file)) {
                    ?>
                    <tr>
                        <td colspan="3">
                            <img src='_inc/folder.png' width='16' height='16' alt='dir'/>
                            <a class="btn-sm btn-info"
                               href="<?php echo $_SERVER['PHP_SELF']; ?>"?path=<?php echo htmlspecialchars($file, ENT_QUOTES, "ISO-8859-1"); ?>
                            ><?php echo $fName; ?>
                            </a>
                        </td>
                    </tr>
                <?php
                }
            }
        }

        closedir($handle);
    }

}

$activePath = isset($_GET['path']) ? $_GET['path'] : $defaultPath;

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=0.85, user-scale=0"/>
    <meta name="description" content="F7 Indexer"/>
    <meta name="author" content="Fahad Hossain"/>
    <link rel="shortcut icon" href="_inc/favicon.ico"/>
    <title><?php echo $activePath; ?> &NestedLessLess; F7 Indexer</title>
    <link href="_inc/bootstrap.css" rel="stylesheet" type="text/css"/>
    <link href="_inc/f7index.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<div class="container">
    <div id="license" class="modal fade in">
        <div class="modal-content container">
            <div class="modal-header">
                <a class="close" data-dismiss="modal">x</a>

                <h3>F7 Indexer | License</h3>
            </div>
            <div class="modal-body">
                <h3>F7 Indexer
                    <small> v1.0c</small>
                </h3>
                <h3 class="text-info">A php script that shows the content of a folder</h3>

                <p>Copyright &copy; &lt;2014&gt; &lt;Fahad Hossain&gt;
                    <br/>This program is free software: you can redistribute it and/or modify it under the terms of the
                    GNU General Public License
                    <br/>
                    as published by the Free Software Foundation, either version 3 of the License,
                    or (at your option) any later version.
                    <br/><br/>
                    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
                    <br/>
                    without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
                    <br/>
                    See the <a href="LICENSE.txt">GNU General Public License</a> for more details.
                    <br/>
                    <br/>
                    You should have received a copy of the GNU General Public License along with this program.
                    If not, see &lt;http://www.gnu.org/licenses/&gt;
                    <br/>
                    <br/>
                    This program uses Twitter Bootstrap for styling the output. Please read their license Agreement
                    before deciding to use it on your version.
                    <br/>
                    <br/>
                    While you are not required to, You can put a link to the original Software and Its author to show
                    the difference between your version and the original.
                </p>
            </div>
            <div class="modal-footer">
                <a href="./LICENSE.txt" class="btn-sm btn-success">Read the complete GPL</a>
                <a href="#" class="btn-sm btn-danger" data-dismiss="modal">Close</a>
            </div>
        </div>
    </div>
    <div class="panel panel-primary">
        <div class="panel-heading"><a class="btn-success btn-sm pull-right"
                                      href="http://<?php echo $_SERVER['HTTP_HOST']; ?>">&leftarrowtail; Back To
                Home</a> Active Directory &rightarrowtail; <a class="btn-info btn-sm"
                                                              href="<?php echo $_SERVER['PHP_SELF']; ?>"><?php echo $activePath; ?></a>
        </div>
        <div class="panel-body">
            <table class="table table-responsive">
                <tr>
                    <th>Name</th>
                    <th>Modified</th>
                    <th class="fsize">File size</th>
                </tr>
                <?php showContent($activePath, $defaultPath); ?>
            </table>
        </div>
        <div class="panel-footer">F7 Indexer
            <span class="pull-right">&copy; <a href="http://twitter.com/devilv15"
                                               class="btn-sm btn-info">@devilv15</a> &middot; <a href="#license"
                                                                                                 class="btn-sm btn-warning"
                                                                                                 title="View License"
                                                                                                 data-toggle="modal">License</a></span>
        </div>
    </div>
</div>
<script type="text/javascript" src="_inc/jquery-latest.js"></script>
<script type="text/javascript" src="_inc/bootstrap.js"></script>
</body>
</html>
