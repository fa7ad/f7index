# F7 Indexer
### A php script that shows the content of a folder


## Usage:

### Define the default path on line 10 ($defaultPath = "/your/path")
* ###This is relative to this file's location
    For example, If
    * this file is in Apache's document root,
    * and the folder to be shown is called 'zeus2'
    * and that 'zeus2' is also on the same directory,  
`$defaultPath` should be `'./zeus2'`

* ####On *nix you can show folders other than the document root by:  
    * Creating a symbolic link in the document root (e.g. /var/www)
    - that points to the actual directory
    -  For example:  
       To display /fahad2,
       ```bash
       sudo ln -sd /fahad2 -t /var/www
       ```
     This will create a symlink called 'fahad2' under /var/www  

     So, now you can display it by setting $defaultPath to './fahad2'  
     * **Provided**, that you(server) have enough permissions to access the actual directory and the _symlink._

* _On Windows similar action can be performed using the "MKLINK" command_
 -  So, to make a symbolic link to your 'D:' drive  
       _under your document root (e.g. C:\xampp\htdocs),_
       - Open an elevated command prompt,  
        - `CD` to your _doc root_  
           And then,
         - `MKLINK /D fahad2 D:\`

        You now have a symlink called 'fahad2' that points to your 'D:' drive.  
       You can now set $defaultPath to './fahad2' just like you would on nix

  **Please note**, that even under windows, Apache (and php and html) uses the Forward slash (`/`) for locations and using a Backslash WILL NOT WORK


# Known Bugs:

During the testing phase of _v1.0b_,
I encountered over a very serious bug surrounding cross-platform compatibility.  


If a **file/folder** has characters other that the **Standard English Alphabet**,  
They get encoded differently causing a 404 error on the server.  
For example:  
* I have a file called `"Bob Taylor - Déjà Vu (ft. Inna).mpg"`  
  * When encoded in a wrong way it appears to be,  
"Bob Taylor - DÃ©jÃ  Vu (ft. Inna).mpg"
  * Which is clearly not the same.

#####One way to overcome this is of course renaming the files but then,  
If you have a lot of files then its a big mess

####So, After a lot of scanning my code for errors,  
I found that changing a <meta> tag is all I had to do.  
* But the problem is,  
  * On Windows, when I set the charset to "utf-8" the problem arises  
and setting it to "us-ascii" fixes it.  
  * But On Linux, it is the flip side, "utf-8" fixes it and "us-ascii" screws it up.  

  So, If you get these weird file names, be sure to check the first meta tag
```html
<meta charset="utf-8"/>
```